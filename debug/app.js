angular.module('app', [])
    .controller('GpioController', ['io', 'PortsRest', '$rootScope', function (io, PortsRest, $rootScope) {
        var that = this;

        that.list = [];
        that.ports = {};

        that.start = function () {
            that.socket = io.start(that.token);

            that.write = function (port, value) {
                that.socket.emit('write', {
                    port: that.writeDataPort,
                    value: that.writeDataValue
                });
            };

            that.socket.on('write', function (data) {
                //change on port write
                that.ports[data.port].value = data.value;
                that.ports[data.port].lastUpdate = new Date();
                $rootScope.$apply();
            });

            that.socket.on('read', function (data) {
                //change on read ort change
                that.ports[data.port].value = data.value;
                that.ports[data.port].lastUpdate = new Date();
                $rootScope.$apply();
            });

            that.socket.onAll(function (action, data) {
                that.list.push({
                    date: new Date(),
                    data: {
                        action: action,
                        data: data
                    }
                });
                $rootScope.$apply();
            });


            PortsRest.get(that.token).then(function (response) {
                that.ports = response.data;
            });

            that.started = true;
        };
    }])
    .service('io', [function () {
        var api = {};

        api.start = function (token) {
            var superEmit, socket, onAllFn;

            socket = io({
                query: 'token=' + token
            });

            socket.onAll = function (cb) {
                onAllFn = cb;
            };

            superEmit = socket.onevent;
            socket.onevent = function () {
                if (onAllFn) {
                    onAllFn.apply(this, arguments);
                }
                superEmit.apply(this, arguments);
            };

            return socket;
        };

        return api;
    }])
    .service('PortsRest', ['$http', function ($http) {
        var api = {};

        api.get = function (token) {
            return $http.get("/rest/ports?token=" + token);
        };

        return api;
    }]);
