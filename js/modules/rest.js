var bodyParser = require('body-parser');
module.error = function (logger, res, msg) {
    logger.error(msg);
    res.json({
        status: "error",
        msg: msg
    }, 403);
};

module.checkReadPermission = function (auth, logger, res, token) {
    if (!token) {
        module.error(logger, res, "No set token");
        return false;
    }

    if (!auth.isReadToken(token)) {
        module.error(logger, res, "Token " + token + " doesn't have read access permission");
        return false;
    }

    return true;
};

module.checkWritePermission = function (auth, logger, res, token) {
    if (!token) {
        module.error(logger, res, "No set token");
        return false;
    }

    if (!auth.isWriteToken(token)) {
        module.error(logger, res, "Token " + token + " doesn't have write access permission");
        return false;
    }

    return true;
};

module.exports = function (config, logger, gpio, app, io, auth) {
    app.use(bodyParser.json());

    app.get('/rest/ports', function (req, res) {
        var portsData = gpio.getPortsData(),
            dtoList = {}, portData;

        if (!module.checkReadPermission(auth, logger, res, req.query.token)) {
            return;
        }

        for (var portName in portsData) {
            portData = portsData[portName];
            dtoList[portName] = {
                mode: portData.mode,
                value: portData.value || portData.defaultValue,
                lastUpdate: portData.lastUpdate || null
            }
        }

        res.json(dtoList);
    });

    app.post('/rest/ports', function (req, res) {
        if (!module.checkWritePermission(auth, logger, res, req.query.token)) {
            return;
        }

        if (!gpio.update(req.body)) {
            module.error(logger, res, "No gpio update");
        }

        res.json({
            status: "success"
        });
    });

    logger.log("rest module... started");
};
