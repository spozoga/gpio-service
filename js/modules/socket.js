module.exports = function (config, logger, gpio, app, io, auth) {
    var values = {};

    gpio.onIn(function (port, value) {
        io.to('read').emit("read", {
            port: port,
            value: value
        });
    });

    gpio.onOut(function (port, value) {
        io.to('read').emit("write", {
            port: port,
            value: value
        });
    });

    io.on('connection', function (socket) {

        if (auth.isReadSocket(socket)) {
            socket.join('read');
        }

        socket.on('write', function (data) {
            if (auth.isWriteSocket(socket)) {
                logger.log("emit", data);
                gpio.emitOut(data.port, data.value);
            } else {
                logger.log("user without role 'write' emit write", socket);
            }
        });

    });

    logger.log("socket module... started");
};
