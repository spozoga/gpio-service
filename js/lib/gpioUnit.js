var gpio = require('rpi-gpio'),
    _ = require('underscore'),
    loggerUnit = require('./loggerUnit'),
    config = require('../../config/config.json'),
    portsData = _.clone(config.gpio),
    defaultRefreshInterval = 200,
    defaultPortValue = 0,
    onIn = [], onOut = [];

// Exports
module.exports.onIn = function (cb) {
    onIn.push(cb);
};

module.exports.onOut = function (cb) {
    onOut.push(cb);
};

module.exports.emitIn = function (port, value) {
    for (var i in onIn) {
        onIn[i](port, value);
    }
};

module.exports.emitOut = function (port, value) {
    for (var i in onOut) {
        onOut[i](port, value);
    }
};

module.exports.update = function (data) {
    var i;
    if (data instanceof Array) {
        for (i = 0; i < data.length; i++) {
            module.exports.emitOut(data[i].port, data[i].value);
        }
    } else {
        if (!data.port) {
            loggerUnit.error("no add port name to request");
            return false;
        }
        module.exports.emitOut(data.port, data.value);
    }
    return true;
};

module.exports.getPortsData = function () {
    return _.clone(portsData);
};

// Helpers

module.transPortMode = function (portConf) {
    return (typeof(portConf.mode) === "string") ? portConf.mode.toLowerCase() : 'r';
};

module.forEachPortWithMode = function (mode, portsData, cb) {
    var i, portMode;
    for (i in portsData) {
        portMode = module.transPortMode(portsData[i]);
        if (portMode === mode) {
            cb(portsData[i], i);
        }
    }
};

module.forEachReadPort = function (portsData, cb) {
    module.forEachPortWithMode('r', portsData, cb);
};

module.forEachWritePort = function (portsData, cb) {
    module.forEachPortWithMode('w', portsData, cb);
};

module.refreshReadPorts = function (portsData) {
    module.forEachReadPort(portsData, function (portData, portName) {
        if (!portData.onReadPort instanceof Function) {
            portData.onReadPort = module.onReadPort.bind(undefined, portName, portData);
        }
        gpio.read(portData.pin, portData.onReadPort);
    });
};

module.onReadPort = function (portName, portData, err, value) {
    if (err) {
        loggerUnit.error('Reading from port ' + portName + ' (pin ' + portData.pin + '): error');
        loggerUnit.error('Reading from port... system modules conflict?? multiple gpio manager?');
        return;
    }
    if (portData.value === value) {
        return;
    }
    portData.value = value;
    module.exports.emitIn(portName, value);
};


module.onOutWriteToPortByDefault = function (portsData, portName, value) {
    var portData = portsData[portName];
    if (!portData) {
        loggerUnit.error("Port " + portName + " not found");
        return;
    }
    gpio.write(portData.pin, value, function (err) {
        if (err) {
            loggerUnit.error('Written to port ' + portName + ' (pin ' + portData.pin + ') error');
        } else {
            loggerUnit.log('Written to port ' + portName + ' (pin ' + portData.pin + ') value: ' + value);
        }
        portData.lastUpdate = new Date();
    });
};

// Init

module.init = function () {

    module.forEachReadPort(portsData, function (portData, portName) {
        gpio.setup(portData.pin, gpio.DIR_IN, function (err) {
            if (err) {
                loggerUnit.error("init read port " + portName, [err]);
                throw err;
            }
        });
    });

    module.forEachWritePort(portsData, function (portData, portName) {
        gpio.setup(portData.pin, gpio.DIR_OUT, function (err) {
            if (err) {
                loggerUnit.error("init write port " + portName, [err]);
                throw err;
            }
            gpio.write(portData.pin, portData.defaultValue || defaultPortValue, function (err) {
                if (err) {
                    loggerUnit.error("error during init port write", [err]);
                    throw err;
                }
            });
        });
    });

    module.exports.onOut(module.onOutWriteToPortByDefault.bind(undefined, portsData));

    setInterval(function () {
        module.refreshReadPorts(portsData);
    }, config.refreshInterval || defaultRefreshInterval);
};

module.init();
