var Server = module.exports = function (config) {
    var app = require('express')(),
        server = require('http').Server(app),
        io = require('socket.io')(server),

        loggerUnit = require("./lib/loggerUnit"),
        gpioUnit = require("./lib/gpioUnit"),

        auth = require("./modules/auth")(config, loggerUnit),
        rest = require("./modules/rest")(config, loggerUnit, gpioUnit, app, io, auth),
        socket = require("./modules/socket")(config, loggerUnit, gpioUnit, app, io, auth);

    if (config.webdebug == true) {
        require("./modules/webdebug")(config, loggerUnit, gpioUnit, app, io);
    }

    //public security
    io.use(function (socket, next) {
        if (auth.auth(socket)) {
            loggerUnit.log("Login success");
            next();
        } else {
            loggerUnit.error("Authentication error");
            next(new Error('Authentication error'));
        }
    });

    // allow origin public
    io.set('origins', '*:*');

    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "X-Requested-With");
        res.header("Access-Control-Allow-Headers", "Content-Type");
        res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
        next();
    });

    server.listen(config.port);
    loggerUnit.log("gpio server started", [config]);
};
