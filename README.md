# gpio-service

It make possible:
 - access to GPIO by sockets
 - run & test your script on remote devices
 - run your app without root permissions
 
# Install
```
git clone https://github.com/SebastianPozoga/gpio-service.git
cd gpio-service
make build
make install
```

needs:
 - Bower
 - Nodejs v0.12.x (or later)
 - Npm
 - make (or you can run: bash -x bin/build.sh & bash -x bin/install.sh)
 
# Service
You can use service like:
```
sudo service gpio start/stop/debug/restart
```

# Standalone
If you want run the script without install you can use:
```
node gpio.js [optional args]
```
Arguments:
 - **--debug** - to show web debug console (default on 8091 port)
 - **--config=$path** - to change config path 

# Usage
You can connect to the service by socket.io (or other socket library). Default port is 8091.
```
io.on('write', function(data) {
  console.log(data.port, data.value);
});

io.on('read', function(data) {
  console.log(data.port, data.value);
});

io.emit('write', {
  port: "my_port_name",
  value: true
});
```
