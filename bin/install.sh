#!/bin/sh

# Make sure only root can run our script
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

#install
rm -rf /otp/services/gpio
mkdir -p /opt/services/gpio
cp -r ./* /opt/services/gpio
rm /otp/services/gpio/bin/install.sh

#add to autostart
cp bin/gpio.sh /etc/init.d/gpio
chmod +x /etc/init.d/gpio
update-rc.d gpio defaults


